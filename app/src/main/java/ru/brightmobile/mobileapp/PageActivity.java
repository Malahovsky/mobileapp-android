package ru.brightmobile.mobileapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.ProgressBar;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;

import ru.brightmobile.mobileapp.lib.firebase.TokenRefreshService;
import ru.brightmobile.mobileapp.lib.location.LocationProvider;

import org.xwalk.core.XWalkActivity;
import org.xwalk.core.XWalkCookieManager;
import org.xwalk.core.XWalkJavascriptResult;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PageActivity extends XWalkActivity
{
    private ProgressBar progressBar;
    private XWalkView xWalkView;

    private boolean isPageLoadStopped = false;

    private ValueCallback<Uri> mUploadFile;
    private File mImageFile;
    private File mVideoFile;

    private HashMap<String, JsonObject> callbacks = new HashMap<String, JsonObject>();

    private static final int PERMISSION_REQUEST_CAMERA = 1;
    private static final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 2;
    private static final int PERMISSION_REQUEST_CAMERA_FOR_QR_CODE = 3;

    private static final int FILE_REQUEST_CODE = 1;

    private Boolean isBackground = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        ((Application) getApplication()).stack.add(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        xWalkView = (XWalkView) findViewById(R.id.xWalkView);

        xWalkView.addJavascriptInterface(new JavascriptInterface(), "exec");

        xWalkView.setResourceClient(new XWalkResourceClient(xWalkView) {

            @Override
            public void onReceivedSslError(XWalkView view, final ValueCallback<Boolean> callback, SslError error)
            {
                new AlertDialog.Builder(PageActivity.this)
                        .setIcon(getApplicationInfo().icon)
                        .setTitle(getApplicationInfo().labelRes)
                        .setMessage(R.string.notification_error_ssl_cert_invalid)
                        .setPositiveButton(R.string.ssl_continue, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                callback.onReceiveValue(true);
                            }

                        })
                        .setNegativeButton(R.string.ssl_cancel, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                callback.onReceiveValue(false);
                            }

                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(XWalkView view, String url)
            {
                Log.e("UrlLoading", url);

                final Uri uri = Uri.parse(url);

                if (!url.equals(view.getOriginalUrl()) && (uri.getScheme().equals("http") || uri.getScheme().equals("https")))
                {
                    if (isPageLoadStopped)
                    {
                        if (uri.getHost().equals(Uri.parse(getString(R.string.app_url)).getHost()))
                        {
                            Intent intent = new Intent(PageActivity.this, PageActivity.class)
                                    .setData(uri);

                            startActivity(intent);
                        }
                        else
                        {
                            CustomTabsIntent intent = new CustomTabsIntent.Builder()
                                    .build();

                            intent.intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                            intent.launchUrl(PageActivity.this, uri);
                        }

                        return true;
                    }
                }

                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onProgressChanged(XWalkView view, int progressInPercent)
            {
                progressBar.setProgress(progressInPercent);
            }

        });

        xWalkView.setUIClient(new XWalkUIClient(xWalkView) {

            @Override
            public boolean onJsAlert(XWalkView view, String url, String message, final XWalkJavascriptResult result)
            {
                if (isBackground)
                {
                    result.cancel();
                }
                else
                {
                    new AlertDialog.Builder(PageActivity.this)
                            .setIcon(getApplicationInfo().icon)
                            .setTitle(getApplicationInfo().labelRes)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i)
                                {
                                    result.confirm();
                                }

                            })
                            .setCancelable(false)
                            .create()
                            .show();
                }

                return true;
            }

            @Override
            public void openFileChooser(XWalkView view, ValueCallback<Uri> uploadFile, String acceptType, String capture)
            {
                mUploadFile = uploadFile;

                String[] permission = new String[] { Manifest.permission.CAMERA };

                if (!hasPermissions(getApplicationContext(), permission))
                {
                    ActivityCompat.requestPermissions(PageActivity.this, permission, PERMISSION_REQUEST_CAMERA);
                }
                else
                {
                    openFile();
                }
            }

            @Override
            public void onPageLoadStopped(XWalkView view, String url, LoadStatus status)
            {
                super.onPageLoadStopped(view, url, status);

                switch (status)
                {
                    case FINISHED:

                        isPageLoadStopped = true;

                        progressBar.setVisibility(View.INVISIBLE);
                        xWalkView.setVisibility(View.VISIBLE);

                        break;

                    case FAILED:

                        new AlertDialog.Builder(PageActivity.this)
                                .setIcon(getApplicationInfo().icon)
                                .setTitle(getApplicationInfo().labelRes)
                                .setMessage("Вы не подключены к Интернету")
                                .setPositiveButton("Повторить", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i)
                                    {
                                        xWalkView.reload(XWalkView.RELOAD_NORMAL);
                                    }

                                })
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i)
                                    {
                                        finish();
                                    }

                                })
                                .setCancelable(false)
                                .create()
                                .show();

                        break;
                }
            }

        });

        if (getSharedPreferences(getPackageName(), Context.MODE_PRIVATE)
                .getInt("firebase_status", 0) == -1)
        {
            startService(new Intent(getApplicationContext(), TokenRefreshService.class));
        }

        LocationProvider.getInstance(this).requestLocationUpdates();
    }

    @Override
    protected void onDestroy()
    {
        ((Application) getApplication()).stack.remove(this);
        super.onDestroy();
    }

    @Override
    protected void onXWalkReady()
    {
        Uri uri = Uri.parse(getString(R.string.app_url));

        if (getIntent().hasExtra("url"))
        {
            uri = Uri.parse(getIntent().getStringExtra("url"));
        }
        else if (getIntent().getData() != null)
        {
            uri = getIntent().getData();

            if (uri.getScheme().equals("appforsale"))
            {
                uri = Uri.parse("http://" + uri.getHost() + uri.getPath());
            }
        }

        XWalkCookieManager cookieManager = new XWalkCookieManager();
        cookieManager.setAcceptCookie(true);
        cookieManager.setAcceptFileSchemeCookies(true);
        cookieManager.setCookie(uri.getScheme() + "://" + uri.getHost(), "MOBILE_DEV=Y");
        cookieManager.setCookie(uri.getScheme() + "://" + uri.getHost(), "MOBILE_DEVICE=android");
        cookieManager.setCookie(uri.getScheme() + "://" + uri.getHost(), "MOBILE_DEVICE_ID=" + Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
        cookieManager.setCookie(uri.getScheme() + "://" + uri.getHost(), "MOBILE_API_VERSION=2");

        // Deprecated
        cookieManager.setCookie(uri.getScheme() + "://" + uri.getHost(), "APPLICATION=N");

        xWalkView.loadUrl(uri.toString());
    }

    private boolean hasPermissions(Context context, String... permissions)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null)
        {
            for (String permission : permissions)
            {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        isBackground = false;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        isBackground = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case FILE_REQUEST_CODE:

                if (data != null && data.getData() != null)
                {
                    mUploadFile.onReceiveValue(data.getData());
                }
                else if (mImageFile != null && mImageFile.length() > 0)
                {
                    mVideoFile.delete();
                    mUploadFile.onReceiveValue(Uri.fromFile(mImageFile));
                }
                else if (mVideoFile != null && mVideoFile.length() > 0)
                {
                    mImageFile.delete();
                    mUploadFile.onReceiveValue(Uri.fromFile(mVideoFile));
                }
                else
                {
                    mImageFile.delete();
                    mVideoFile.delete();
                    mUploadFile.onReceiveValue(null);
                }

                mUploadFile = null;
                mImageFile = null;
                mVideoFile = null;

                break;

            case IntentIntegrator.REQUEST_CODE:

                if (resultCode == RESULT_OK)
                {
                    callBackExecute(callbacks.get("getQRCode").get("callback").getAsInt(), "{CODE:'" + data.getStringExtra("SCAN_RESULT") + "'}");
                }

                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    class JavascriptInterface
    {
        private Handler handler = new Handler();

        @org.xwalk.core.JavascriptInterface
        public void postMessage(final String command, final String sParams)
        {
            handler.post(new Runnable() {

                @SuppressLint("MissingPermission")
                @Override
                public void run()
                {
                    Log.e("postMessage", command + ": " + sParams);

                    JsonObject params = new GsonBuilder().create().fromJson(sParams, JsonObject.class);

                    switch (command)
                    {
                        case "getCurrentPosition":

                            if (params.has("success"))
                            {
                                registerCallBack("getCurrentPosition", params);

                                String[] permission = new String[] { Manifest.permission.ACCESS_FINE_LOCATION };

                                if (!hasPermissions(getApplicationContext(), permission))
                                {
                                    ActivityCompat.requestPermissions(PageActivity.this, permission, PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
                                }
                                else
                                {
                                    getCurrentPosition();
                                }
                            }

                            break;

                        case "historyBack":

                            finish();

                            break;

                        case "getQRCode":

                            if (params.has("callback"))
                            {
                                registerCallBack("getQRCode", params);

                                String[] permission = new String[] { Manifest.permission.CAMERA };

                                if (!hasPermissions(getApplicationContext(), permission))
                                {
                                    ActivityCompat.requestPermissions(PageActivity.this, permission, PERMISSION_REQUEST_CAMERA_FOR_QR_CODE);
                                }
                                else
                                {
                                    getQRCode();
                                }
                            }

                            break;
                    }
                }

            });
        }
    }

    @SuppressLint("MissingPermission")
    private void getCurrentPosition()
    {
        if (callbacks.containsKey("getCurrentPosition"))
        {
            final JsonObject params = callbacks.get("getCurrentPosition");

            if (params.has("success"))
            {
                LocationRequest request = LocationRequest.create()
                        .setNumUpdates(1);

                if (params.has("options"))
                {
                    JsonObject options = params.getAsJsonObject("options");

                    if (options.has("enableHighAccuracy") && options.get("enableHighAccuracy").getAsBoolean())
                    {
                        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    }

                    if (options.has("timeout"))
                    {
                        request.setMaxWaitTime(options.get("timeout").getAsLong());
                    }

                    if (options.has("maximumAge"))
                    {
                        request.setExpirationDuration(options.get("maximumAge").getAsLong());
                    }
                }

                LocationServices.getFusedLocationProviderClient(getApplicationContext())
                        .requestLocationUpdates(request, new LocationCallback() {

                            @Override
                            public void onLocationResult(LocationResult locationResult)
                            {
                                Location location = locationResult.getLastLocation();
                                if (location != null)
                                {
                                    callBackExecute(params.get("success").getAsInt(), "{ coords: { latitude: " + location.getLatitude() + ", longitude: " + location.getLongitude() + ", accuracy: " + location.getAccuracy() + " }, timestamp: " + location.getTime() + " }");
                                }
                            }

                        }, Looper.myLooper());
            }
        }
    }

    private void openFile()
    {
        Intent target = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("*/*");

        Intent chooser = Intent.createChooser(target, "Выберите действие");

        if (hasPermissions(getApplicationContext(), new String[] { Manifest.permission.CAMERA }))
        {
            List<Intent> extra = new ArrayList<Intent>();

            try
            {
                String prefix = new SimpleDateFormat("yyyyMMdd_HHmmss")
                        .format(new Date());
                mImageFile = File.createTempFile("IMG_" + prefix, ".jpg",
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES));
                mVideoFile = File.createTempFile("VID_" + prefix, ".mp4",
                        getExternalFilesDir(Environment.DIRECTORY_MOVIES));
            }
            catch (IOException e)
            {

            }

            if (mImageFile != null)
            {
                extra.add(
                        new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                .putExtra(MediaStore.EXTRA_OUTPUT, getUriForFile(mImageFile))
                );
            }

            if (mVideoFile != null)
            {
                extra.add(
                        new Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                            .putExtra(MediaStore.EXTRA_OUTPUT, getUriForFile(mVideoFile))
                );
            }

            if (!extra.isEmpty())
            {
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extra.toArray(new Parcelable[extra.size()]));
            }
        }

        startActivityForResult(chooser, FILE_REQUEST_CODE);
    }

    private Uri getUriForFile(File file)
    {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
        {
            return FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", file);
        }
        else
        {
            return Uri.fromFile(file);
        }
    }

    private void getQRCode()
    {
        new IntentIntegrator(this)
                .initiateScan();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CAMERA:

                openFile();

                break;

            case PERMISSION_REQUEST_ACCESS_FINE_LOCATION:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    getCurrentPosition();

                    LocationProvider.getInstance(this).requestLocationUpdates();
                }
                else if (callbacks.containsKey("getCurrentPosition") && callbacks.get("getCurrentPosition").has("error"))
                {
                    callBackExecute(callbacks.get("getCurrentPosition").get("error").getAsInt(), "{code: 1, message: \"PERMISSION_DENIED\"}");
                }

                break;

            case PERMISSION_REQUEST_CAMERA_FOR_QR_CODE:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    getQRCode();
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void registerCallBack(String name, JsonObject params)
    {
        callbacks.put(name, params);
    }

    private void callBackExecute(Integer index, String result)
    {
        xWalkView.loadUrl("javascript:BM.MobileApp.callBackExecute(" + index + ", " + result + ")");
    }
}