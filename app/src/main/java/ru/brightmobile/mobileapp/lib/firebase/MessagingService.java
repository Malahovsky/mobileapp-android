package ru.brightmobile.mobileapp.lib.firebase;

import android.content.Context;
import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;

public class MessagingService extends FirebaseMessagingService
{
    @Override
    public void onNewToken(String token)
    {
        getSharedPreferences(getPackageName(), Context.MODE_PRIVATE)
                .edit()
                .putInt("firebase_status", -1)
                .putString("firebase_token", token)
                .apply();

        startService(new Intent(getApplicationContext(), TokenRefreshService.class));
    }
}