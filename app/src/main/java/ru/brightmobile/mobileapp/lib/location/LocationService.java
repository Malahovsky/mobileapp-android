package ru.brightmobile.mobileapp.lib.location;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.location.LocationResult;

import ru.brightmobile.mobileapp.R;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LocationService extends Service
{
    private OkHttpClient httpClient;

    @Override
    public void onCreate()
    {
        httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId)
    {
        if (LocationResult.hasResult(intent))
        {
            LocationResult locationResult = LocationResult.extractResult(intent);
            Location location = locationResult.getLastLocation();
            if (location != null)
            {
                Uri uri = Uri.parse(getString(R.string.app_url));

                RequestBody body = new FormBody.Builder()
                        .add("longitude", Double.toString(location.getLongitude()))
                        .add("latitude", Double.toString(location.getLatitude()))
                        .add("accuracy", Double.toString(location.getAccuracy()))
                        .add("timestamp", Long.toString(location.getTime()))
                        .build();

                Request request = new Request.Builder()
                        .url(uri.getScheme() + "://" + uri.getHost() + "/bitrix/tools/mlab_appforsale/set_current_position.php")
                        .post(body)
                        .build();

                httpClient.newCall(request).enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, IOException e)
                    {
                        stopSelf(startId);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException
                    {
                        stopSelf(startId);
                    }

                });
            }
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}