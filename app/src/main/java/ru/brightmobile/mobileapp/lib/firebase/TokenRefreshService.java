package ru.brightmobile.mobileapp.lib.firebase;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import ru.brightmobile.mobileapp.R;

public class TokenRefreshService extends Service
{
    private OkHttpClient httpClient;

    @Override
    public void onCreate()
    {
        httpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId)
    {
        final SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

        if (sharedPreferences.getInt("firebase_status", 0) == -1
                && sharedPreferences.getString("firebase_token", null) != null)
        {
            sharedPreferences.edit()
                    .putInt("firebase_status", 0)
                    .apply();

            Uri uri = Uri.parse(getString(R.string.app_url));

            RequestBody body = new FormBody.Builder()
                    .add("app_id", getPackageName())
                    .add("device_id", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID))
                    .add("device_model", Build.BRAND + " " + Build.MODEL)
                    .add("system_version", Build.VERSION.RELEASE)
                    .add("token", sharedPreferences.getString("firebase_token", ""))
                    .build();

            Request request = new Request.Builder()
                    .url(uri.getScheme() + "://" + uri.getHost() + "/bitrix/tools/mlab_appforsale/register_device.php")
                    .post(body)
                    .build();

            httpClient.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, IOException e)
                {
                    sharedPreferences.edit()
                            .putInt("firebase_status", -1)
                            .apply();

                    stopSelf(startId);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try
                    {
                        if (new Gson().fromJson(response.body().charStream(), JsonObject.class)
                                .get("response").getAsInt() == 1)
                        {
                            sharedPreferences.edit()
                                    .putInt("firebase_status", 1)
                                    .remove("firebase_token")
                                    .apply();

                            stopSelf(startId);
                        }
                        else
                        {
                            onFailure(call, new IOException());
                        }
                    }
                    catch (Exception e)
                    {
                        onFailure(call, new IOException(e));
                    }
                }

            });

        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}